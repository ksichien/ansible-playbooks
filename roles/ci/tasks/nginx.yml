---
# file: roles/ci/tasks/nginx.yml
- name: install openssl prerequisities
  apt:
    name: python3-openssl
    state: latest
- name: create ssl directory
  file:
    path: /etc/nginx/ssl/
    state: directory
- name: Check that the certificate exists
  stat:
    path: /etc/nginx/ssl/{{ server.name }}.crt
  register: self_signed_crt
- name: Check that the key exists
  stat:
    path: /etc/nginx/ssl/{{ server.name }}.pem
  register: self_signed_key
- name: generate an openssl private key
  openssl_privatekey:
    path: "/etc/nginx/ssl/{{ server.name }}.pem"
    passphrase: "{{ certificate.passphrase }}"
    cipher: aes256
  when: self_signed_key.stat.exists == False
- name: generate an openssl csr
  openssl_csr:
    path: "/etc/nginx/ssl/{{ server.name }}.csr"
    privatekey_path: "/etc/nginx/ssl/{{ server.name }}.pem"
    privatekey_passphrase: "{{ certificate.passphrase }}"
    common_name: "{{ certificate.common }}"
    state: present
  when: self_signed_crt.stat.exists == False
- name: generate an openssl self-signed certificate
  openssl_certificate:
    provider: selfsigned
    csr_path: "/etc/nginx/ssl/{{ server.name }}.csr"
    path: "/etc/nginx/ssl/{{ server.name }}.crt"
    privatekey_path: "/etc/nginx/ssl/{{ server.name }}.pem"
    privatekey_passphrase: "{{ certificate.passphrase }}"
    state: present
  when: self_signed_crt.stat.exists == False
- name: save the certificate passphrase to a file for nginx
  copy:
    content: "{{ certificate.passphrase }}"
    dest: "/etc/nginx/ssl/{{ server.name }}.pass"
    mode: 0600
- name: install nginx
  apt:
    name: ['nginx']
    state: latest
- name: remove default nginx website
  file:
    path: /etc/nginx/sites-enabled/default
    state: absent
  notify: restart nginx
- name: create jenkins conf file
  template:
    src: "{{ item }}.conf"
    dest: "/etc/nginx/sites-enabled/{{ item }}.conf"
  with_items: ['jenkins']
  notify: restart nginx
- name: make sure nginx is running and enabled on startup
  service:
    name: nginx
    state: started
    enabled: yes
